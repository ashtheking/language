package org.serakos.language;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;

import org.serakos.language.definition.Restriction;
import org.serakos.language.definition.Structure;
import org.serakos.language.definition.ortho.Consonants;
import org.serakos.language.definition.ortho.Vowels;
import org.serakos.language.definition.phoneme.Phoneme;

public class Language
{
	public Random rand;
	public Structure structure;
	public Map<Phoneme.Type, List<? extends Phoneme>> phonemes;
	public Restriction restriction;
	public Vowels vowelOrtho;
	public Consonants consonantOrtho;
	public String genitive;
	public String definite;
	public String joiner;
	public int minCharacters;
	public int maxCharacters;
	public int syllablesPerMorpheme;
	public int charactersPerENMorph;

	private Set<String> genericMorphemes;
	private Map<String, String> morphemes;
	private Map<String, String> words;
	private Map<String, String> names;

	/**
	 * Creates a default English-like language.
	 */
	public Language()
	{
		this.rand = new Random();
		this.structure = Structure.CVC;
		this.phonemes = new HashMap<>(Phoneme.Type.values().length);
		this.restriction = Restriction.DOUBLE_AND_HARD_CLUSTERS;
		this.vowelOrtho = Vowels.Ácutes;
		this.consonantOrtho = Consonants.Default;
		this.joiner = "-";
		this.minCharacters = 3;
		this.maxCharacters = 12;
		this.syllablesPerMorpheme = 1;
		this.charactersPerENMorph = 3;
		this.genericMorphemes = new HashSet<>();
		this.morphemes = new HashMap<>();
		this.words = new HashMap<>();
		this.names = new HashMap<>();
	}

	/**
	 * Defines a language.
	 * 
	 * @param struct
	 *            The Structure for the syllables composing the language
	 * @param types
	 *            The array of ints defining what set is used for each phoneme.
	 * @param rest
	 *            The restrictions on the generated syllables, if any.
	 * @param vortho
	 *            The vowel orthography to use.
	 * @param cortho
	 *            The consonant orthography to use.
	 */
	public Language(Structure struct, Map<Phoneme.Type, List<? extends Phoneme>> types, Restriction rest,
			Vowels vortho, Consonants cortho)
	{
		this();
		this.structure = struct;
		this.phonemes = types;
		this.restriction = rest;
		this.vowelOrtho = vortho;
		this.consonantOrtho = cortho;
	}

	public void initializeStandards() {
		genitive = getMorpheme("of");
		definite = getMorpheme("the");
		try {
			List<String> nouns;
			Path path = Paths.get("nounlist.txt");
			if (path.toFile().exists())
				nouns = Files.readAllLines(path);
			else {
				nouns = new ArrayList<>();
				try (BufferedReader r = new BufferedReader(
						new InputStreamReader(Language.class.getResourceAsStream("/nounlist.txt")))) {
					while (r.ready())
						nouns.add(r.readLine());
				}
			}
			nouns.stream().forEach(this::getWord);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * Generator methods
	 */

	private String spell(String syllable, List<Phoneme.Type> used) {
		StringBuilder str = new StringBuilder();
		char[] split = syllable.toCharArray();
		for (int i = 0; i < split.length; i++) {
			String ortho = (used.get(i).isConsonant ? consonantOrtho : vowelOrtho).map("" + split[i]);
			str.append(ortho == null ? split[i] : ortho);
		}
		return str.toString();
	}

	private String makeSyllable() {
		StringBuilder str;
		List<Phoneme.Type> used;
		do {
			str = new StringBuilder();
			used = new ArrayList<>();
			for (int i = 0; i < structure.types.size(); i++) {
				Phoneme.Type type = structure.types.get(i);
				if (i + 1 < structure.types.size() && structure.types.get(i + 1) == Phoneme.Type.OPTIONAL) {
					i++;
					if (rand.nextBoolean())
						continue;
				}
				str.append(getRand(phonemes.get(type), rand).getChar());
				used.add(type);
			}
		} while (restriction.test(str.toString()));
		return spell(str.toString(), used);
	}

	private String getMorpheme(String key) {
		if (morphemes.containsKey(key))
			return morphemes.get(key);
		int index = rand.nextInt(genericMorphemes.size() + 5);
		if (index >= genericMorphemes.size()) {
			String morph = "";
			for (int x = 0; x < syllablesPerMorpheme; x++)
				morph += makeSyllable();
			morphemes.put(key, morph);
			genericMorphemes.add(morph);
			return morph;
		}
		return genericMorphemes.toArray(new String[genericMorphemes.size()])[index];
	}

	private String makeWord(String key) {
		List<String> bits = splitEqually(key, charactersPerENMorph);
		StringBuilder str = new StringBuilder();
		for (String s : bits)
			str.append(getMorpheme(s));
		return str.toString();
	}

	public String getWord(String key) {
		if (words.containsKey(key))
			return words.get(key);
		String word = makeWord(key);
		words.put(key, word);
		return word;
	}

	private Pattern wordPattern = Pattern.compile("\\w+");
	private Pattern boundaryPattern = Pattern.compile("\\b");

	public String translate(String input) {
		List<String> arr = Arrays.asList(boundaryPattern.split(input));
		List<Boolean> indexes = new ArrayList<>();
		for (int x = 0; x < arr.size(); x++)
			indexes.add(wordPattern.matcher(arr.get(x).trim()).matches());

		StringBuilder out = new StringBuilder();
		for (int x = 0; x < arr.size(); x++) {
			String orig = arr.get(x);
			if (indexes.get(x)) {
				String tran = getWord(orig.trim().toLowerCase());
				if (Character.isUpperCase(orig.charAt(0)))
					tran = capitalize(tran);
				out.append(tran);
			}
			else {
				out.append(orig);
			}
		}
		return out.toString();
	}

	public String makeName() {
		String name = null;
		String[] keys = new String[2];
		do {
			keys[0] = getRand(words, rand);
			if (rand.nextBoolean())
				name = getWord(keys[0]);
			else {
				do {
					keys[1] = getRand(words, rand);
				} while (keys[1].equals(keys[0]));
				String w1 = getWord(keys[0]);
				String w2 = getWord(keys[1]);
				if (rand.nextBoolean())
					name = w1 + joiner + w2;
				else
					name = w1 + joiner + genitive + joiner + w2;
			}
			if (rand.nextDouble() < 0.1)
				name = definite + joiner + name;
		} while (name.length() < minCharacters || name.length() > maxCharacters);
		return name;
	}

	public String getName(String key) {
		if (names.containsKey(key))
			return names.get(key);
		String word = makeName();
		names.put(key, word);
		return word;
	}

	/*
	 * Getter methods
	 */

	public Set<String> getGenericMorphemes() {
		return genericMorphemes;
	}

	public Map<String, String> getMorphemes() {
		return morphemes;
	}

	public Map<String, String> getWords() {
		return words;
	}

	public Map<String, String> getNames() {
		return names;
	}

	/*
	 * Other
	 */

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("[");
		str.append(structure);
		str.append(", ");
		str.append(restriction);
		str.append(", ");
		str.append(consonantOrtho);
		str.append(", ");
		str.append(vowelOrtho);
		for (Phoneme.Type type : Phoneme.Type.values()) {
			if (type == Phoneme.Type.OPTIONAL)
				continue;
			str.append(", ");
			str.append(phonemes.get(type));
		}
		str.append("]");
		return str.toString();
	}

	/**
	 * Rolls an entirely random language.
	 * 
	 * @return The created language.
	 */
	public static Language randomLanguage() {
		Random r = new Random();
		Language lang = new Language(Structure.getRandom(r), Phoneme.getRandom(r), Restriction.getRandom(r),
				Vowels.getRandom(r), Consonants.getRandom(r));
		lang.rand = r;
		lang.joiner = r.nextBoolean() ? " " : "-";
		return lang;
	}

	/*
	 * Static helper methods
	 */

	public static String capitalize(String word) {
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}

	public static List<String> splitEqually(String text, int size) {
		if (text.length() < size + 2)
			return Arrays.asList(text);
		// thanks @http://stackoverflow.com/a/3760193
		List<String> ret = new ArrayList<>((text.length() + size - 1) / size);
		for (int start = 0; start < text.length(); start += size)
			ret.add(text.substring(start, Math.min(text.length(), start + size)));
		return ret;
	}

	public static <T> T getRand(T[] arr, Random rand) {
		return arr[rand.nextInt(arr.length)];
	}

	public static <T> T getRand(List<T> arr, Random rand) {
		return arr.get(rand.nextInt(arr.size()));
	}

	public static <T> List<T> getRandSet(T[] arr, Random rand) {
		return getRandSet(arr, rand, 1);
	}

	public static <T> List<T> getRandSet(T[] arr, Random rand, int bias) {
		int num = rand.nextInt(arr.length) + bias;
		List<T> list = new ArrayList<>();
		for (int x = 0; x < num; x++)
			list.add(getRand(arr, rand));
		return list;
	}

	public static <T, V> T getRand(Map<T, V> map, Random rand) {
		Set<T> keys = map.keySet();
		if (map.size() == 0)
			return null;
		int num = rand.nextInt(map.size());
		Iterator<T> itr = keys.iterator();
		for (int x = 0; x < num; x++)
			itr.next();
		return itr.next();
	}
}