package org.serakos.language.definition;

import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.serakos.language.Language;

public enum Restriction
{
	NONE(),
	DOUBLE_SOUNDS("(.)\\1"),
	DOUBLE_AND_HARD_CLUSTERS("[sʃf][sʃ]", "(.)\\1", "[rl][rl]");

	private List<Matcher> matchers;

	Restriction(String... regexes)
	{
		matchers = Stream.of(regexes) // Stream
				.map(Pattern::compile) // Convert to pattern
				.map(p -> p.matcher("")) // Get empty matcher
				.collect(Collectors.toList()); // Collect
	}

	public boolean test(String syllable) {
		return matchers.stream() // Stream
				.map(m -> m.reset(syllable)) // Reset
				.filter(Matcher::matches) // Find matches
				.findAny().isPresent(); // if one exists
	}

	public String getPatterns() {
		return matchers.stream().map(m -> m.pattern()).collect(Collectors.toList()).toString();
	}

	public static Restriction getRandom(Random random) {
		return Language.getRand(values(), random);
	}
}
