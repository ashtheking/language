package org.serakos.language.definition.ortho;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.serakos.language.Language;

public enum Vowels implements Orthography
{
	Ácutes("á", "é", "í", "ó", "ú"),
	Ümlauts("ä", "ë", "ï", "ö", "ü"),
	Welsh("â", "ê", "y", "ô", "w"),
	Diphthongs("au", "ei", "ie", "ou", "oo"),
	Doubles("aa", "ee", "ii", "oo", "uu");

	private Map<String, String> map = new HashMap<>();

	Vowels(String... s)
	{
		map.put("A", s[0]);
		map.put("E", s[1]);
		map.put("I", s[2]);
		map.put("O", s[3]);
		map.put("U", s[4]);
	}

	@Override
	public String map(String input) {
		return map.get(input);
	}

	@Override
	public String getMap() {
		return map.toString();
	}

	public static Vowels getRandom(Random random) {
		return Language.getRand(values(), random);
	}
}
