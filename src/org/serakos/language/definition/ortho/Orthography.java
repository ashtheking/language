package org.serakos.language.definition.ortho;

public interface Orthography
{
	public String map(String input);

	public String getMap();
}
