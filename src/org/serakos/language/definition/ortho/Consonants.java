package org.serakos.language.definition.ortho;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.serakos.language.Language;

public enum Consonants implements Orthography
{
	Default(),
	Slavic("ʃ", "š", "ʒ", "ž", "ʧ", "č", "ʤ", "ǧ", "j", "j"),
	German("ʃ", "sch", "ʒ", "zh", "ʧ", "tsch", "ʤ", "dz", "j", "j", "x", "ch"),
	French("ʃ", "ch", "ʒ", "j", "ʧ", "tch", "ʤ", "dj", "x", "kh"),
	Pinyin("ʃ", "x", "ʧ", "q", "ʤ", "j");

	private Map<String, String> map = new HashMap<>();

	Consonants(String... s)
	{
		defaults();
		for (int x = 0; x < s.length; x += 2)
			map.put(s[x], s[x + 1]);
	}

	private void defaults() {
		map.put("ʃ", "sh");
		map.put("ʒ", "zh");
		map.put("ʧ", "ch");
		map.put("ʤ", "j");
		map.put("ŋ", "ng");
		map.put("j", "y");
		map.put("x", "kh");
		map.put("ɣ", "gh");
		map.put("ɲ", "gn");
		map.put("ʁ", "re");
		map.put("ʔ", "‘");
	}

	@Override
	public String map(String input) {
		return map.get(input);
	}

	@Override
	public String getMap() {
		return map.toString();
	}

	public static Consonants getRandom(Random random) {
		return Language.getRand(values(), random);
	}
}
