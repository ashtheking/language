package org.serakos.language.definition;

import static org.serakos.language.definition.phoneme.Phoneme.Type.CONSONANT;
import static org.serakos.language.definition.phoneme.Phoneme.Type.FINAL;
import static org.serakos.language.definition.phoneme.Phoneme.Type.LIQUID;
import static org.serakos.language.definition.phoneme.Phoneme.Type.OPTIONAL;
import static org.serakos.language.definition.phoneme.Phoneme.Type.SIBILANT;
import static org.serakos.language.definition.phoneme.Phoneme.Type.VOWEL;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.serakos.language.Language;
import org.serakos.language.definition.phoneme.Phoneme;

public enum Structure
{
	CVC(CONSONANT, VOWEL, CONSONANT),
	CVVOC(CONSONANT, VOWEL, VOWEL, OPTIONAL, CONSONANT),
	CVVCO(CONSONANT, VOWEL, VOWEL, CONSONANT, OPTIONAL),
	CVCO(CONSONANT, VOWEL, CONSONANT, OPTIONAL),
	CV(CONSONANT, VOWEL),
	VC(VOWEL, CONSONANT),
	CVF(CONSONANT, VOWEL, FINAL),
	COVC(CONSONANT, OPTIONAL, VOWEL, CONSONANT),
	CVFO(CONSONANT, VOWEL, FINAL, OPTIONAL),
	CLOVC(CONSONANT, LIQUID, OPTIONAL, VOWEL, CONSONANT),
	CLOVF(CONSONANT, LIQUID, OPTIONAL, VOWEL, FINAL),
	SOCV(SIBILANT, OPTIONAL, CONSONANT, VOWEL, CONSONANT),
	SOCVF(SIBILANT, OPTIONAL, CONSONANT, VOWEL, FINAL),
	SOCVCO(SIBILANT, OPTIONAL, CONSONANT, VOWEL, CONSONANT, OPTIONAL),
	COVF(CONSONANT, OPTIONAL, VOWEL, FINAL),
	COVCO(CONSONANT, OPTIONAL, VOWEL, CONSONANT, OPTIONAL),
	COVFO(CONSONANT, OPTIONAL, VOWEL, FINAL, OPTIONAL),
	COLOVC(CONSONANT, OPTIONAL, LIQUID, OPTIONAL, VOWEL, CONSONANT),
	CVLOCO(CONSONANT, VOWEL, LIQUID, OPTIONAL, CONSONANT, OPTIONAL),
	COVLOC(CONSONANT, OPTIONAL, VOWEL, LIQUID, OPTIONAL, CONSONANT),
	COVLCO(CONSONANT, OPTIONAL, VOWEL, LIQUID, CONSONANT, OPTIONAL);

	public List<Phoneme.Type> types;

	Structure(Phoneme.Type... t)
	{
		this.types = Arrays.asList(t);
	}

	public String getStructure() {
		return types.toString();
	}

	public static Structure getRandom(Random random) {
		return Language.getRand(values(), random);
	}
}
