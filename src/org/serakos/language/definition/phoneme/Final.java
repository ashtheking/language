package org.serakos.language.definition.phoneme;

import java.util.List;
import java.util.Random;

import org.serakos.language.Language;

public enum Final implements Phoneme
{
	K("k"),
	M("m"),
	N("n"),
	S("s"),
	Z("z"),
	ŋ("ŋ"),
	ʃ("ʃ"),
	ʒ("ʒ");

	private String character;

	Final(String s)
	{
		character = s;
	}

	@Override
	public Type getType() {
		return Type.FINAL;
	}

	@Override
	public String getChar() {
		return character;
	}

	@Override
	public int ord() {
		return ordinal();
	}

	public static List<Final> getRandom(Random random) {
		return Language.getRandSet(values(), random);
	}
}
