package org.serakos.language.definition.phoneme;

import java.util.List;
import java.util.Random;

import org.serakos.language.Language;

public enum Vowel implements Phoneme
{
	A("A"),
	E("E"),
	I("I"),
	O("O"),
	U("U"),
	a("a"),
	e("e"),
	i("i"),
	o("o"),
	u("u");

	private String character;

	Vowel(String s)
	{
		character = s;
	}

	@Override
	public Type getType() {
		return Type.VOWEL;
	}

	@Override
	public String getChar() {
		return character;
	}

	@Override
	public int ord() {
		return ordinal();
	}

	public static List<Vowel> getRandom(Random random) {
		return Language.getRandSet(values(), random);
	}
}
