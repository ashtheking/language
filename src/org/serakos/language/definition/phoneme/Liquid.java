package org.serakos.language.definition.phoneme;

import java.util.List;
import java.util.Random;

import org.serakos.language.Language;

public enum Liquid implements Phoneme
{
	R("r"),
	L("l"),
	W("w"),
	J("j");

	private String character;

	Liquid(String s)
	{
		character = s;
	}

	@Override
	public Type getType() {
		return Type.LIQUID;
	}

	@Override
	public String getChar() {
		return character;
	}

	@Override
	public int ord() {
		return ordinal();
	}

	public static List<Liquid> getRandom(Random random) {
		return Language.getRandSet(values(), random);
	}
}
