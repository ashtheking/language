package org.serakos.language.definition.phoneme;

import java.util.List;
import java.util.Random;

import org.serakos.language.Language;

public enum Consonant implements Phoneme
{
	B("b"),
	D("d"),
	G("g"),
	H("h"),
	J("j"),
	K("k"),
	L("l"),
	M("m"),
	N("n"),
	P("p"),
	Q("q"),
	R("r"),
	S("s"),
	T("t"),
	V("v"),
	W("w"),
	X("x"),
	Z("z"),
	ŋ("ŋ"), // NG - si_ng_
	ɲ("ɲ"), // GN - a_gn_eau (French)
	ɣ("ɣ"), // GH - gamma (Greek)
	ʁ("ʁ"), // RE - _r_ed
	ʃ("ʃ"), // SH - _sh_eep
	ʒ("ʒ"), // ZH - mea_su_re
	ʔ("ʔ"), // __ - uh_-_oh
	ʧ("ʧ"); // CH - _ch_ip

	private String character;

	Consonant(String s)
	{
		character = s;
	}

	@Override
	public Type getType() {
		return Type.CONSONANT;
	}

	@Override
	public String getChar() {
		return character;
	}

	@Override
	public int ord() {
		return ordinal();
	}

	public static List<Consonant> getRandom(Random random) {
		return Language.getRandSet(values(), random, values().length / 3);
	}

	/*
	 *
	 * MINIMAL("p", "t", "k", "m", "n", "l", "s"),
	 * ENGLISH("p", "t", "k", "b", "d", "g", "m", "n", "l", "r", "s", "ʃ", "z", "ʒ", "ʧ"),
	 * PIRAHA("p", "t", "k", "m", "n", "h"),
	 * HAWAIIAN("h", "k", "l", "m", "n", "p", "w", "ʔ"),
	 * GREENLANDIC("p", "t", "k", "q", "v", "s", "g", "r", "m", "n", "ŋ", "l", "j"),
	 * ARABIC("t", "k", "s", "ʃ", "d", "b", "q", "ɣ", "x", "m", "n", "l", "r", "w", "j"),
	 * ARABIC_LITE("t", "k", "d", "g", "m", "n", "s", "ʃ"),
	 * ENGLISH_LITE("p", "t", "k", "b", "d", "g", "m", "n", "s", "z", "ʒ", "ʧ", "h", "j", "w");
	 */
}
