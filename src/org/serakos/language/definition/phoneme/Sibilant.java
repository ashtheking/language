package org.serakos.language.definition.phoneme;

import java.util.List;
import java.util.Random;

import org.serakos.language.Language;

public enum Sibilant implements Phoneme
{
	S("s"),
	Z("z"),
	ʃ("ʃ"),
	ʧ("ʧ"),
	ʤ("ʤ"),
	ʒ("ʒ"),
	// Yes this also includes Stridents!
	f("f"),
	v("v");

	private String character;

	Sibilant(String s)
	{
		character = s;
	}

	@Override
	public Type getType() {
		return Type.SIBILANT;
	}

	@Override
	public String getChar() {
		return character;
	}

	@Override
	public int ord() {
		return ordinal();
	}

	public static List<Sibilant> getRandom(Random random) {
		return Language.getRandSet(values(), random);
	}
}
