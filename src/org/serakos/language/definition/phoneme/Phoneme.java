package org.serakos.language.definition.phoneme;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

public interface Phoneme
{
	public static final Phoneme OPT = () -> "";

	public String getChar();

	public default Type getType() {
		return Type.OPTIONAL;
	}

	public default int ord() {
		return 0;
	}

	public static Map<Type, List<? extends Phoneme>> getRandom(Random r) {
		HashMap<Phoneme.Type, List<? extends Phoneme>> randPhonemes = new HashMap<>();
		for (Phoneme.Type type : Phoneme.Type.values())
			randPhonemes.put(type, type.getRand.apply(r));
		return randPhonemes;
	}

	public static Map<Type, List<? extends Phoneme>> getStandard() {
		HashMap<Phoneme.Type, List<? extends Phoneme>> map = new HashMap<>();
		map.put(Type.CONSONANT,
				Arrays.asList(Consonant.P, Consonant.T, Consonant.K, Consonant.B, Consonant.D, Consonant.G,
						Consonant.M, Consonant.N, Consonant.L, Consonant.R, Consonant.S, Consonant.ʃ,
						Consonant.Z, Consonant.ʒ, Consonant.ʧ));
		map.put(Type.LIQUID, Arrays.asList(Liquid.R, Liquid.L));
		map.put(Type.SIBILANT, Arrays.asList(Sibilant.S));
		map.put(Type.FINAL, Arrays.asList(Final.M, Final.N));
		map.put(Type.VOWEL, Arrays.asList(Vowel.a, Vowel.e, Vowel.i, Vowel.o, Vowel.u));
		map.put(Type.OPTIONAL, Arrays.asList(OPT));
		return map;
	}

	public static enum Type
	{
		CONSONANT(true, Consonant::values, Consonant::getRandom),
		LIQUID(true, Liquid::values, Liquid::getRandom),
		SIBILANT(true, Sibilant::values, Sibilant::getRandom),
		FINAL(true, Final::values, Final::getRandom),
		VOWEL(false, Vowel::values, Vowel::getRandom),
		OPTIONAL(false, () -> new Phoneme[] { OPT }, (r) -> Arrays.asList(OPT));

		public boolean isConsonant;
		public Supplier<? extends Phoneme[]> values;
		public Function<Random, List<? extends Phoneme>> getRand;

		Type(boolean b, Supplier<? extends Phoneme[]> v, Function<Random, List<? extends Phoneme>> f)
		{
			this.isConsonant = b;
			this.values = v;
			this.getRand = f;
		}
	}
}