package org.serakos.language.example;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import org.serakos.language.Language;
import org.serakos.language.definition.Restriction;
import org.serakos.language.definition.Structure;
import org.serakos.language.definition.ortho.Consonants;
import org.serakos.language.definition.ortho.Orthography;
import org.serakos.language.definition.ortho.Vowels;
import org.serakos.language.definition.phoneme.Consonant;
import org.serakos.language.definition.phoneme.Final;
import org.serakos.language.definition.phoneme.Liquid;
import org.serakos.language.definition.phoneme.Phoneme;
import org.serakos.language.definition.phoneme.Phoneme.Type;
import org.serakos.language.definition.phoneme.Sibilant;
import org.serakos.language.definition.phoneme.Vowel;

public class LanguageDesigner
{
	protected JFrame frame;
	protected JList<Structure> structure;
	protected List<JList<Phoneme>> phonemes;
	protected JList<Restriction> restriction;
	protected JList<Vowels> orthV;
	protected JList<Consonants> orthC;
	protected JList<String> name;
	protected JTextArea output;
	protected JTextArea input;

	protected Language language;
	private JScrollPane outputScroll;
	private JScrollPane inputScroll;
	private JButton btnRandom;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					UIManager.setLookAndFeel("com.jtattoo.plaf.graphite.GraphiteLookAndFeel");
					LanguageDesigner window = new LanguageDesigner();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LanguageDesigner()
	{
		Map<Phoneme.Type, List<? extends Phoneme>> defaultMap = new HashMap<>();
		defaultMap.put(Type.CONSONANT, Arrays.asList(Consonant.P, Consonant.T, Consonant.K, Consonant.M,
				Consonant.N, Consonant.L, Consonant.S));
		defaultMap.put(Type.FINAL, Arrays.asList(Final.M, Final.N));
		defaultMap.put(Type.LIQUID, Arrays.asList(Liquid.R, Liquid.L));
		defaultMap.put(Type.SIBILANT, Arrays.asList(Sibilant.S));
		defaultMap.put(Type.VOWEL, Arrays.asList(Vowel.a, Vowel.e, Vowel.i, Vowel.o, Vowel.u));
		defaultMap.put(Type.OPTIONAL, Arrays.asList());

		Language englishlikeLang = new Language(Structure.CVC, defaultMap, Restriction.NONE, Vowels.Ácutes,
				Consonants.Default);
		englishlikeLang.rand = new Random();
		englishlikeLang.joiner = " ";

		initialize();
		randomLanguage(englishlikeLang);
		updateLanguage();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 900, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(3, 1, 0, 10));

		phonemes = new ArrayList<>();

		setupConfig();

		JPanel inputPanel = new JPanel();
		frame.getContentPane().add(inputPanel);
		inputPanel.setLayout(new GridLayout(0, 2, 70, 0));

		JScrollPane nameScroll = new JScrollPane();
		nameScroll.setViewportBorder(
				new TitledBorder(null, "Name List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		inputPanel.add(nameScroll);

		name = new JList<>();
		nameScroll.setViewportView(name);

		outputScroll = new JScrollPane();
		frame.getContentPane().add(outputScroll);

		output = new JTextArea();
		output.setWrapStyleWord(true);
		output.setLineWrap(true);
		output.setEditable(false);
		outputScroll.setViewportView(output);

		inputScroll = new JScrollPane();
		inputPanel.add(inputScroll);

		input = new JTextArea();
		input.setWrapStyleWord(true);
		input.setLineWrap(true);
		input.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent paramKeyEvent) {
				output.setText(language.translate(input.getText()));
			}
		});
		try {
			input.setText(Files.lines(Paths.get("declaration.txt")).reduce("", (a, b) -> a + b + "\n"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		inputScroll.setViewportView(input);
	}

	private void setupConfig() {
		JPanel configPanel = new JPanel();
		frame.getContentPane().add(configPanel);
		configPanel.setLayout(new GridLayout(0, 6, 5, 5));

		JScrollPane structureScroll = new JScrollPane();
		configPanel.add(structureScroll);

		structure = new JList<>();
		structure.setListData(Structure.values());
		structure.setSelectedIndex(0);
		setTooltip(structure);
		structureScroll.setViewportView(structure);
		structureScroll.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Syllable Structure", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		structure.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		for (Phoneme.Type type : Phoneme.Type.values())
			if (type != Phoneme.Type.OPTIONAL) {
				JScrollPane phoScroll = new JScrollPane();
				phoScroll.setBorder(
						new TitledBorder(UIManager.getBorder("TitledBorder.border"), type.name() + " Phoneme",
								TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
				configPanel.add(phoScroll);

				JList<Phoneme> pho = new JList<>();
				pho.setListData(type.values.get());
				setTooltip(pho);
				pho.setSelectedIndex(0);
				pho.setVisibleRowCount(1);
				pho.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
				phonemes.add(pho);

				phoScroll.setViewportView(pho);
			}

		JScrollPane restrictionScroll = new JScrollPane();
		configPanel.add(restrictionScroll);

		restriction = new JList<>();
		restriction.setListData(Restriction.values());
		setTooltip(restriction);
		restriction.setSelectedIndex(0);
		restrictionScroll.setViewportView(restriction);
		restrictionScroll.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Sound Restrictions", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		restriction.setVisibleRowCount(1);
		restriction.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane orthVScroll = new JScrollPane();
		configPanel.add(orthVScroll);

		orthV = new JList<>();
		orthV.setListData(Vowels.values());
		setTooltip(orthV);
		orthV.setSelectedIndex(0);
		orthVScroll.setViewportView(orthV);
		orthVScroll.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Vowel Orthography", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		orthV.setVisibleRowCount(1);
		orthV.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JScrollPane orthCScroll = new JScrollPane();
		configPanel.add(orthCScroll);

		orthC = new JList<>();
		orthC.setListData(Consonants.values());
		setTooltip(orthC);
		orthC.setSelectedIndex(0);
		orthCScroll.setViewportView(orthC);
		orthCScroll.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"Consonant Orthography", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		orthC.setVisibleRowCount(1);
		orthC.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		btnRandom = new JButton("Random");
		btnRandom.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent paramMouseEvent) {
				randomLanguage(null);
				updateLanguage();
			}
		});
		configPanel.add(btnRandom);

		JButton btnGenerate = new JButton("Generate");
		btnGenerate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent paramMouseEvent) {
				setLanguage();
				updateLanguage();
			}
		});
		configPanel.add(btnGenerate);
	}

	public void setLanguage() {
		Map<Phoneme.Type, List<? extends Phoneme>> map = new HashMap<>();
		for (JList<Phoneme> phos : phonemes)
			map.put(phos.getSelectedValue().getType(), phos.getSelectedValuesList());
		map.put(Phoneme.Type.OPTIONAL, Arrays.asList(Phoneme.OPT));
		language = new Language(structure.getSelectedValue(), map, restriction.getSelectedValue(),
				orthV.getSelectedValue(), orthC.getSelectedValue());
	}

	public void randomLanguage(Language lang) {
		language = lang == null ? Language.randomLanguage() : lang;

		structure.setSelectedValue(language.structure, true);

		List<List<Integer>> lists = Stream.of(Phoneme.Type.values()) // For each Type
				.map(language.phonemes::get) // Get the List
				.map(t -> t.stream().map(Phoneme::ord).collect(Collectors.toList())) // Get List of indicies
				.collect(Collectors.toList());
		for (int x = 0; x < phonemes.size(); x++) {
			Integer[] in = lists.get(x).toArray(new Integer[0]);
			int[] i = new int[in.length];
			for (int k = 0; k < i.length; k++)
				i[k] = in[k];
			phonemes.get(x).setSelectedIndices(i);
		}

		restriction.setSelectedValue(language.restriction, true);

		orthV.setSelectedValue(language.vowelOrtho, true);
		orthC.setSelectedValue(language.consonantOrtho, true);
	}

	public void updateLanguage() {
		language.initializeStandards();
		List<String> names = Stream.generate(language::makeName) // Make names
				.limit(50) // Make only 50
				.map(Language::capitalize) // Capitalize
				.collect(Collectors.toList()); // Collect
		name.setListData(names.toArray(new String[names.size()]));
		output.setText(language.translate(input.getText()));

	}

	private static <T> void setTooltip(JList<T> list) {
		list.addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				ListModel<T> m = list.getModel();
				int index = list.locationToIndex(e.getPoint());
				if (index > -1)
					list.setToolTipText(getTooltip(m.getElementAt(index)));
			}

			public String getTooltip(T element) {
				if (Orthography.class.isAssignableFrom(element.getClass()))
					return ((Orthography) element).getMap();
				if (Phoneme.class.isAssignableFrom(element.getClass()))
					return ((Phoneme) element).getChar();
				if (Structure.class.isAssignableFrom(element.getClass()))
					return ((Structure) element).getStructure();
				if (Restriction.class.isAssignableFrom(element.getClass()))
					return ((Restriction) element).getPatterns();
				return "";
			}
		});
	}
}
