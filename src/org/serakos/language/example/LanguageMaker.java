package org.serakos.language.example;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import org.serakos.language.Language;
import org.serakos.language.definition.Restriction;

public class LanguageMaker
{
	public static void main(String[] args) throws Exception {
		List<String> nouns = Files.readAllLines(Paths.get("nounlist.txt"));

		Language language = Language.randomLanguage();
		language.restriction = Restriction.DOUBLE_AND_HARD_CLUSTERS;
		language.initializeStandards();
		nouns.stream().forEach(language::translate);
		System.out.println("Language: ");
		// System.out.println("\tMorphemes: " + language.getMorphemes());
		List<String> names = new ArrayList<>();
		for (int k = 0; k < 50; k++)
			names.add(language.makeName());
		System.out.println("\tNames: " + names);
		System.out.println("\t---");
		try (Scanner scan = new Scanner(System.in)) {
			Stream.generate(scan::nextLine)// Scan
					//@formatter:off
					.map(t -> {if(t.equals("/stop")) System.exit(0); return t;}) //Exit
					//@formatter:on
					.map(language::translate) // Translate
					.forEach(System.out::println);
		}
	}
}
