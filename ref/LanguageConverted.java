package org.serakos.language;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.serakos.language.definition.Restriction;
import org.serakos.language.definition.Structure;
import org.serakos.language.definition.ortho.Consonants;
import org.serakos.language.definition.ortho.Vowels;
import org.serakos.language.definition.phoneme.Phoneme;

public class LanguageConverted
{
	private Random rand;
	private Structure structure;
	private int[] phonemes;
	private Restriction restriction;
	private Vowels vowelOrtho;
	private Consonants consonantOrtho;
	private String genitive;
	private String definite;
	private String joiner;
	private int minSyllables;
	private int maxSyllables;
	private int minCharacters;
	private int maxCharacters;

	private Map<String, List<String>> morphemes;
	private Map<String, List<String>> words;
	private List<String> names;

	/**
	 * Creates a default English-like language.
	 */
	public LanguageConverted()
	{
		this.rand = new Random();
		this.structure = Structure.CVC;
		this.phonemes = new int[Phoneme.values().length];
		this.restriction = Restriction.DOUBLE_AND_HARD_CLUSTERS;
		this.vowelOrtho = Vowels.Ácutes;
		this.consonantOrtho = Consonants.Default;
		this.joiner = "-";
		this.minSyllables = 1;
		this.maxSyllables = 4;
		this.minCharacters = 5;
		this.maxCharacters = 12;
		this.morphemes = new HashMap<>();
		this.words = new HashMap<>();
		this.names = new ArrayList<>();
	}

	/**
	 * Defines a language.
	 * 
	 * @param struct
	 *            The Structure for the syllables composing the language
	 * @param types
	 *            The array of ints defining what set is used for each phoneme.
	 * @param rest
	 *            The restrictions on the generated syllables, if any.
	 * @param vortho
	 *            The vowel orthography to use.
	 * @param cortho
	 *            The consonant orthography to use.
	 */
	public LanguageConverted(Structure struct, int[] types, Restriction rest, Vowels vortho,
			Consonants cortho)
	{
		this();
		this.structure = struct;
		this.phonemes = types;
		this.restriction = rest;
		this.vowelOrtho = vortho;
		this.consonantOrtho = cortho;
	}

	/*
	 * Generator methods
	 */

	private String spell(String syllable, List<Phoneme> used) {
		StringBuilder str = new StringBuilder();
		String[] split = syllable.split("");
		for (int i = 0; i < split.length; i++) {
			String ortho = (used.get(i).isConsonant ? consonantOrtho : vowelOrtho).map(split[i]);
			str.append(ortho == null ? split[i] : ortho);
		}
		return str.toString();
	}

	private String makeSyllable() {
		StringBuilder str;
		List<Phoneme> used;
		do {
			str = new StringBuilder();
			used = new ArrayList<>();
			for (int i = 0; i < structure.types.size(); i++) {
				Phoneme type = structure.types.get(i);
				if (i + 1 < structure.types.size() && structure.types.get(i + 1) == Phoneme.OPT) {
					i++;
					if (rand.nextBoolean())
						continue;
				}
				str.append(type.getChar(phonemes[type.ordinal()], rand));
				used.add(type);
			}
		} while (restriction.test(str.toString()));
		return spell(str.toString(), used);
	}

	private String getMorpheme(String key) {
		List<String> list = getMorphemeList(key);
		int extra = key.equals("") ? 10 : 1;
		String morph;
		loop: while (true) {
			int n = rand.nextInt(list.size() + extra);
			if (n < list.size())
				return list.get(n);
			morph = makeSyllable();
			for (String k : morphemes.keySet())
				if (morphemes.get(k).contains(morph))
					continue loop;
			list.add(morph);
			morphemes.put(key, list);
			return morph;
		}
	}

	private String makeWord(String key) {
		int numSyll = randrange(rand, minSyllables, maxSyllables + 1);
		StringBuilder str = new StringBuilder();
		int randNum = rand.nextInt(numSyll);
		for (int x = 0; x < numSyll; x++)
			str.append(getMorpheme(x == randNum ? key : ""));
		return str.toString();
	}

	public String getWord(String key) {
		List<String> list = getWordList(key);
		int extra = key.equals("") ? 3 : 2;
		String word;
		do {
			int n = rand.nextInt(list.size() + extra);
			if (n < list.size())
				return list.get(n);
			word = makeWord(key);
		} while (words.containsValue(word));
		list.add(word);
		words.put(key, list);
		return word;
	}

	public String makeName(String key) {
		genitive = genitive == null ? getMorpheme("of") : genitive;
		definite = definite == null ? getMorpheme("the") : definite;
		String name = null;
		loop: while (true) {
			if (rand.nextBoolean())
				name = capitalize(getWord(key));
			else {
				String w1 = capitalize(getWord(rand.nextDouble() < 0.6 ? key : ""));
				String w2 = capitalize(getWord(rand.nextDouble() < 0.6 ? key : ""));
				if (w1.equalsIgnoreCase(w2))
					continue loop;
				if (rand.nextBoolean())
					name = w1 + joiner + w2;
				else
					name = w1 + joiner + genitive + joiner + w2;
			}
			if (rand.nextDouble() < 0.1)
				name = definite + joiner + name;
			if (name.length() < minCharacters || name.length() > maxCharacters)
				continue loop;
			for (String n : names)
				if (n.contains(name) || name.contains(n))
					continue loop;
			names.add(name);
			return name;
		}
	}

	/*
	 * Getter methods
	 */

	public List<String> getMorphemeList(String key) {
		if (!morphemes.containsKey(key))
			morphemes.put(key, new ArrayList<>());
		return Collections.unmodifiableList(morphemes.get(key));
	}

	public List<String> getWordList(String key) {
		if (!words.containsKey(key))
			words.put(key, new ArrayList<>());
		return Collections.unmodifiableList(words.get(key));
	}

	/*
	 * Setter methods
	 */

	public void setRandom(Random random) {
		this.rand = random;
	}

	public void setCharacterLimits(int min, int max) {
		this.minCharacters = min;
		this.maxCharacters = max;
	}

	public void setSyllableLimits(int min, int max) {
		this.minSyllables = min;
		this.maxSyllables = max;
	}

	public void setJoiner(String j) {
		this.joiner = j;
	}

	/*
	 * Other
	 */

	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("[");
		str.append(structure);
		str.append(", ");
		str.append(restriction);
		str.append(", ");
		str.append(consonantOrtho);
		str.append(", ");
		str.append(vowelOrtho);
		for (Phoneme type : Phoneme.values()) {
			if (type == Phoneme.OPT)
				continue;
			str.append(", ");
			str.append(type.getName(phonemes[type.ordinal()]));
		}
		str.append("]");
		return str.toString();
	}

	/**
	 * Rolls an entirely random language.
	 * 
	 * @return The created language.
	 */
	public static LanguageConverted randomLanguage() {
		Random r = new Random();
		LanguageConverted lang = new LanguageConverted(Structure.getRandom(r), Phoneme.getRandomTypes(r),
				Restriction.getRandom(r), Vowels.getRandom(r), Consonants.getRandom(r));
		lang.rand = r;
		lang.minSyllables = randrange(r, 1, 3);
		lang.maxSyllables = randrange(r, lang.minSyllables + 1, 7);
		lang.joiner = r.nextBoolean() ? " " : "-";
		return lang;
	}

	/*
	 * Static helper methods
	 */

	private static String capitalize(String word) {
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}

	private static int randrange(Random rand, int lo, int hi) {
		return rand.nextInt(hi - lo) + lo;
	}
}
